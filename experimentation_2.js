var express = require('express');

var app = express();

app.get('/', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.end('Vous êtes à l\'accueil, que puis-je pour vous ?');
})

.get('/boutique/:rayon', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.end('Vous êtes dans le rayon ' + req.params.rayon);
})

.get('/arriere', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.end('Vous êtes à l\'arrière boutique, sortez d\'ici !');
})

.get('/etage1/stock', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.end('Il n\'y a rien à voir ici...');
})

.get('/etage2/sallerepos', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.end('On fait une pause ?');
})

.get('/etage3/coffre', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.end('On est riche mais il ne faut le dire.');
})

.use(function(req, res, next){
    res.setHeader('Content-Type', 'text/plain');
    res.send(404, 'La princesse est dans un autre château...');
});

app.listen(8080);