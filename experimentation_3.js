var express = require('express');
var ejs = require('ejs');

var app = express();

app.get('/compteur/:nombre', function(req, res) {
    var noms = ['Robert', 'Jacques', 'David'];
    res.render('compteur.ejs', {compteur: req.params.nombre, noms: noms});
});

app.listen(8080);