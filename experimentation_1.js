var http = require('http');
var url = require("url");
var querystring = require('querystring');
var EventEmitter = require('events').EventEmitter;
var express = require('express');
var markdown = require('markdown').markdown;
var module = require('./module');

var test = new EventEmitter();

var server = http.createServer(function(req, res) {
  var page = url.parse(req.url).pathname;
  var params = querystring.parse(url.parse(req.url).query);
  url.parse(req.url).pathname;
  console.log(page);
  res.writeHead(200, {"Content-Type": "text/html"});
  if (page == '/page') {
    if ('prenom' in params && 'nom' in params) {
      
      test.on('smalltext', function(firstname,lastname) {
        console.log(firstname, lastname);
      });

      test.emit('smalltext', params['prenom'], params['nom']);

      module.direBonjour();
      module.direByeBye();
      
    }
    else {
        res.write('Vous devez bien avoir un prénom et un nom, non ?');
    }
    }
  else {
    res.write('La page demandée n\'éxiste pas');
  }
  res.end();
});

var app = express();

app.get('/', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.end('Vous êtes à l\'accueil');
});

console.log(markdown.toHTML('Un paragraphe en **markdown** !'));

server.listen(8080);